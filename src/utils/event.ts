import { Dom } from '@/core'

export default (event: Event): DomElement | null => {
    const $target = event.target
    if ($target instanceof HTMLElement) {
        return new Dom($target)
    }
    return null
}
