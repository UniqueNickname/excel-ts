export const range = (start: number, end: number): number[] => {
    const [rangeStart, rangeEnd] = start > end ? [end, start] : [start, end]
    return new Array(rangeEnd - rangeStart + 1)
        .fill('')
        .map((_: '', index: number) => rangeStart + index)
}

export const cellSelectorTemplate = (
    col: string | number,
    row: string | number,
): string => `[data-col="${col}"][data-row="${row}"]`

export const getCells = (
    $current: DomElement,
    $target: DomElement,
    $root: DomElement,
): DomElement[] => {
    if (
        !$current.data.col ||
        !$current.data.row ||
        !$target.data.col ||
        !$target.data.row
    )
        return []
    const cols = range(+$current.data.col, +$target.data.col)
    const rows = range(+$current.data.row, +$target.data.row)
    const selectors: string[] = cols.reduce((acc: string[], col) => {
        rows.forEach(row => acc.push(cellSelectorTemplate(col, row)))
        return acc
    }, [])

    const $cells: DomElement[] = []
    selectors.forEach(selector => {
        const $cell = $root.find(selector)
        if ($cell) $cells.push($cell)
    })
    return $cells
}

export const getSelectorOfNextCell = (
    key: string,
    col: number,
    row: number,
): string => {
    let [newCol, newRow] = [col, row]
    switch (key) {
        case 'Enter':
        case 'ShiftArrowDown':
            newRow += 1
            break
        case 'Tab':
        case 'ShiftArrowRight':
            newCol += 1
            break
        case 'ShiftArrowUp':
            newRow -= 1
            break
        case 'ShiftTab':
        case 'ShiftArrowLeft':
            newCol -= 1
            break

        default:
            break
    }
    return cellSelectorTemplate(newCol, newRow)
}
