import { AbstractDomListener } from '@/core'
import { capitalize } from '@/utils'

interface ActiveListener {
    type: EventType
    callback: EventListenerOrEventListenerObject
}

const getMethodName = (eventName: EventType): keyof AbstractDomListener =>
    `on${capitalize(eventName)}` as keyof AbstractDomListener

export default class DomListener {
    protected $root: DomElement

    private listeners: EventType[]

    private name: string

    private activeListeners: ActiveListener[] = []

    constructor($root: DomElement, listeners: EventType[], name: string) {
        this.$root = $root
        this.listeners = listeners
        this.name = name
    }

    initDomListeners(): void {
        this.listeners.forEach(listenerType => {
            const method = getMethodName(listenerType)
            const listenerMethod = this[method] as DomListenerMethod | undefined
            if (!listenerMethod) {
                throw new Error(
                    `Method ${method} is not implemented in ${this.name} component`,
                )
            }
            const eventListener = listenerMethod.bind(this)
            this.activeListeners.push({
                type: listenerType,
                callback: eventListener,
            })
            this.$root.on(listenerType, eventListener)
        })
    }

    removeDomListeners(): void {
        this.activeListeners.forEach(listener => {
            this.$root.off(listener.type, listener.callback)
        })
        this.activeListeners = []
    }
}
