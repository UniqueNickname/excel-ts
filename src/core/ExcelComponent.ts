import { DomListener } from '@/core'

export default class ExcelComponent
    extends DomListener
    implements IClassComponent {
    private emitter: IEmitter

    private unsubscribes: IUnsubscribe[] = []

    constructor($root: DomElement, options: ExcelComponentOptions) {
        super($root, options.listeners || [], options.name || '')
        this.emitter = options.emitter
    }

    toHTML(): string {
        return ''
    }

    $emit(eventName: string, arg: unknown = undefined): void {
        this.emitter.emit(eventName, arg)
    }

    $subscribe(eventName: string, callback: IListener): void {
        const unsubscribe = this.emitter.subscribe(eventName, callback)
        this.unsubscribes.push(unsubscribe)
    }

    init(): void {
        this.initDomListeners()
    }

    destroy(): void {
        this.removeDomListeners()
        this.unsubscribes.forEach(unsubscribe => {
            unsubscribe()
        })
    }
}
