export default class Dom implements DomElement {
    private $el: HTMLElement | HTMLInputElement

    constructor(selector: string | HTMLElement) {
        if (typeof selector === 'string') {
            const $el = document.querySelector(selector)
            if ($el) this.$el = $el as HTMLElement
            else throw new Error('No element with the selector')
        } else this.$el = selector
    }

    get data(): DOMStringMap {
        return this.$el.dataset
    }

    get text(): string {
        return this.$el.textContent?.trim() || ''
    }

    set text(value: string) {
        if (this.$el instanceof HTMLInputElement) this.$el.value = value
        else this.$el.textContent = value
    }

    html(html: string): DomElement | string {
        this.$el.innerHTML = html
        return this
    }

    clear(): DomElement {
        this.html('')
        return this
    }

    append(node: HTMLElement | Dom): DomElement {
        this.$el.appendChild(node instanceof HTMLElement ? node : node.$el)
        return this
    }

    closest(selector: string): DomElement | null {
        const element = this.$el.closest(selector)
        if (element instanceof HTMLElement) return new Dom(element)
        return null
    }

    findAll(selector: string): DomElement[] {
        const NodeList = this.$el.querySelectorAll(selector)
        const cells: DomElement[] = []
        NodeList.forEach(cell => cells.push(new Dom(cell as HTMLElement)))
        return cells
    }

    find(selector: string): DomElement | null {
        const element = this.$el.querySelector(selector) as HTMLElement | null
        return element ? new Dom(element) : null
    }

    getCoords(): DOMRect {
        return this.$el.getBoundingClientRect()
    }

    focus(): DomElement {
        this.$el.focus()
        document.execCommand('selectAll', false)
        document.getSelection()?.collapseToEnd()
        return this
    }

    addClass(classes: string): DomElement {
        this.$el.classList.add(classes)
        return this
    }

    removeClass(classes: string): DomElement {
        this.$el.classList.remove(classes)
        return this
    }

    css(styles: CSS): DomElement {
        const styleNames = Object.keys(styles) as Array<keyof CSS>
        styleNames.forEach(styleName => {
            this.$el.style[styleName] = styles[styleName]!
        })
        return this
    }

    on(
        eventType: EventType,
        callback: EventListenerOrEventListenerObject,
    ): DomElement {
        this.$el.addEventListener(eventType, callback)
        return this
    }

    off(
        eventType: EventType,
        callback: EventListenerOrEventListenerObject,
    ): DomElement {
        this.$el.removeEventListener(eventType, callback)
        return this
    }

    static create(tagName: string, classes = ''): DomElement {
        const el: HTMLElement = document.createElement(tagName)
        if (classes) el.classList.add(classes)
        return new Dom(el)
    }
}
