export default class Emitter implements IEmitter {
    listeners: IListeners = {}

    emit(eventName: string, arg: unknown = undefined): void {
        if (!Array.isArray(this.listeners[eventName])) return
        this.listeners[eventName].forEach(listener => listener(arg))
    }

    subscribe(eventName: string, callback: IListener): IUnsubscribe {
        this.listeners[eventName] = this.listeners[eventName] || []
        this.listeners[eventName].push(callback)
        return () => {
            this.listeners[eventName] = this.listeners[eventName].filter(
                listener => listener !== callback,
            )
        }
    }
}
