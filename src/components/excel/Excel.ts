import { Dom, Emitter } from '@/core'

interface ExcelOptions {
    components: ClassComponent[]
}

export default class Excel {
    private $el: DomElement

    private ClassComponents: ClassComponent[]

    private components: IClassComponent[] = []

    emitter: Emitter

    constructor(selector: string, options: ExcelOptions) {
        const $el = new Dom(selector)
        if ($el) this.$el = $el
        else throw new Error('Element not found')
        this.ClassComponents = options.components || []
        this.emitter = new Emitter()
    }

    getRoot(): DomElement {
        const $root = Dom.create('div', 'excel')
        const componentOptions: ExcelComponentOptions = {
            emitter: this.emitter,
        }
        this.components = this.ClassComponents.map(Class => {
            const $el = Dom.create('div', Class.className)
            const component = new Class($el, componentOptions)
            $el.html(component.toHTML())
            $root.append($el)
            return component
        })
        return $root
    }

    render(): void {
        this.$el.append(this.getRoot())
        this.components.forEach(component => component.init())
    }

    destroy(): void {
        this.components.forEach(component => {
            component.destroy()
        })
    }
}
