import { getCells, getEventTarget } from '@/utils'

export const resizeTable = (
    mousedown: MouseEvent,
    $root: DomElement,
): boolean => {
    const $resizeTool = getEventTarget(mousedown)
    if (!$resizeTool) return false
    const resizeDirection = $resizeTool.data.resize
    if (!resizeDirection) return false

    const $parent = $resizeTool.closest('[data-type="resizable"]')
    if (!$parent) return false

    const parentCoords = $parent.getCoords()
    const $cells = $root.findAll(`[data-col="${$parent.data.col}"]`)

    let size: number

    document.onmousemove = mousemove => {
        if (resizeDirection === 'col') {
            size = mousemove.pageX - parentCoords.right + parentCoords.width
            $parent.css({ width: `${size}px` })
        } else {
            size = mousemove.pageY - parentCoords.bottom + parentCoords.height
            $parent.css({ height: `${size}px` })
        }
    }

    document.onmouseup = () => {
        document.onmousemove = null
        document.onmouseup = null
        if (resizeDirection === 'col') {
            $cells.forEach($cell => {
                $cell.css({ width: `${size}px` })
            })
            $resizeTool.css({ right: '' })
        } else {
            $parent.css({ height: `${size}px` })
            $resizeTool.css({ bottom: '' })
        }
    }
    return true
}

export const selectCell = (
    event: MouseEvent,
    selection: ITableSelection,
    $root: DomElement,
): boolean => {
    const $target = getEventTarget(event)
    if (!$target) return false
    if ($target.data.type !== 'cell' || !$target.data.col || !$target.data.row)
        return false
    if (event.shiftKey && selection.$current) {
        selection.selectGroupe(getCells(selection.$current, $target, $root))
    } else selection.select($target)
    return true
}
