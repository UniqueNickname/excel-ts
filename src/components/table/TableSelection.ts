export default class TableSelection implements ITableSelection {
    private selectClass = 'excel-table__cell_selected'

    groupe: DomElement[] = []

    $current: DomElement | undefined

    clear(): void {
        this.groupe
            .splice(0)
            .forEach(element => element.removeClass(this.selectClass))
    }

    select($cell: DomElement): DomElement {
        this.clear()
        this.groupe = [
            (this.$current = $cell.addClass(this.selectClass).focus()),
        ]
        return this.$current
    }

    selectGroupe($cells: DomElement[]): void {
        this.clear()
        this.groupe = $cells
        this.groupe.forEach($cell => {
            $cell.addClass(this.selectClass)
        })
    }
}
