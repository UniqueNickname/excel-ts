const CODES = {
    A: 65,
    Z: 90,
}

const toCell = (row: number) => (_: string, column: number) =>
    `<div
        class="excel-table__cell"
        contenteditable
        data-row="${row}"
        data-col="${column}"
        data-type="cell"
    ></div>`

const createCell = (row: number, count: number) =>
    Array(count).fill('').map(toCell(row)).join('')

const toRow = (info: string, cells: string) => {
    const resize = info
        ? '<div class="excel-table__row-resize" data-resize="row"></div>'
        : ''
    const data = info ? 'data-type="resizable"' : ''

    return `
        <div class="excel-table__row" ${data}>
            <div class="excel-table__row-info">
                ${info}
                ${resize}
            </div>
            <div class="excel-table__row-data">${cells}</div>
        </div>
    `
}

const createRow = (columnsCount: number) => (_: string, row: number) =>
    toRow((row + 1).toString(), createCell(row, columnsCount))

const toColumn = (tag: string, index: number) =>
    `
        <div class="excel-table__column" data-type="resizable" data-col="${index}">
            ${tag}
            <div class="excel-table__column-resize" data-resize="col"></div>
        </div>
    `

const toChar = (_: string, index: number) =>
    String.fromCharCode(CODES.A + index)

export default (rowsCount = 15): string => {
    const columnsCount = CODES.Z - CODES.A + 1
    const columns = Array(columnsCount)
        .fill('')
        .map(toChar)
        .map(toColumn)
        .join('')

    const rows = Array(rowsCount).fill('').map(createRow(columnsCount))
    rows.unshift(toRow('', columns))
    return rows.join('')
}
