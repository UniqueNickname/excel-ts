import { ExcelComponent } from '@/core'
import { getEventTarget, getSelectorOfNextCell } from '@/utils'
import { resizeTable, selectCell } from './table.functions'
import createTable from './table.template'
import TableSelection from './TableSelection'

export default class Table extends ExcelComponent {
    static className = 'excel-table'

    private selection: TableSelection = new TableSelection()

    constructor($root: DomElement, options: ExcelComponentOptions) {
        super($root, {
            name: 'Table',
            listeners: ['mousedown', 'keydown', 'input'],
            ...options,
        })
    }

    toHTML(): string {
        return createTable(100)
    }

    init(): void {
        super.init()
        const $cell = this.$root.find('[data-col="0"][data-row="0"]')
        if ($cell) this.selectCell($cell)
        this.$subscribe('formula:input', (text: string) => {
            if (this.selection.$current) this.selection.$current.text = text
        })
        this.$subscribe('formula:enter', () => {
            if (this.selection.$current) this.selection.$current.focus()
        })
    }

    private selectCell($cell: DomElement): void {
        this.$emit('table:select', this.selection.select($cell))
    }

    onMousedown(mousedown: MouseEvent): void {
        if (resizeTable(mousedown, this.$root)) return
        if (selectCell(mousedown, this.selection, this.$root))
            this.selectCell(this.selection.$current as DomElement)
    }

    onKeydown(keydown: KeyboardEvent): void {
        const keys = [
            'Enter',
            'Tab',
            'ShiftTab',
            'ShiftArrowUp',
            'ShiftArrowDown',
            'ShiftArrowLeft',
            'ShiftArrowRight',
        ]

        if (
            !this.selection.$current ||
            !this.selection.$current.data.col ||
            !this.selection.$current.data.row
        )
            return

        const key = keydown.shiftKey ? `Shift${keydown.key}` : keydown.key

        if (keys.includes(key)) {
            keydown.preventDefault()
            const $next = this.$root.find(
                getSelectorOfNextCell(
                    key,
                    +this.selection.$current.data.col,
                    +this.selection.$current.data.row,
                ),
            )
            if ($next) this.selectCell($next)
        }
    }

    onInput(event: InputEvent): void {
        this.$emit('table:input', getEventTarget(event))
    }
}
