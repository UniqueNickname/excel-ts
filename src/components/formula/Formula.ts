import { ExcelComponent } from '@/core'
import { getEventTarget } from '@/utils'

export default class Formula extends ExcelComponent {
    static className = 'excel-formula'

    constructor($root: DomElement, options: ExcelComponentOptions) {
        super($root, {
            name: 'Formula',
            listeners: ['input', 'keydown'],
            ...options,
        })
    }

    toHTML(): string {
        return `
            <div class="excel-formula__info">fx</div>
            <div
                class="excel-formula__input"
                contenteditable
                spellcheck="false"
                data-type="formula"
            ></div>
        `
    }

    init(): void {
        super.init()
        const $formula = this.$root.find('[data-type="formula"]')
        this.$subscribe('table:select', ($cell: DomElement) => {
            if ($formula) $formula.text = $cell.text
        })
        this.$subscribe('table:input', ($cell: DomElement) => {
            if ($formula) $formula.text = $cell.text
        })
    }

    onInput(event: InputEvent): void {
        this.$emit('formula:input', getEventTarget(event)?.text)
    }

    onKeydown(event: KeyboardEvent): void {
        const keys = ['Enter', 'Tab']
        if (keys.includes(event.key)) {
            event.preventDefault()
            this.$emit('formula:enter')
        }
    }
}
