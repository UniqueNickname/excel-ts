interface DomElement {
    data: DOMStringMap

    text: string

    html(html: string | undefined): DomElement | string

    clear(): DomElement

    append(node: HTMLElement | Dom): DomElement

    closest(selector: string): DomElement | null

    findAll(selector: string): DomElement[]

    find(selector: string): DomElement | null

    getCoords(): DOMRect

    focus(): DomElement

    addClass(classes: string): DomElement

    removeClass(classes: string): DomElement

    css(styles: CSS): DomElement

    on(
        eventType: EventType,
        callback: EventListenerOrEventListenerObject,
    ): DomElement

    off(
        eventType: EventType,
        callback: EventListenerOrEventListenerObject,
    ): DomElement
}
