interface ClassComponent {
    className: string

    new ($root: DomElement, options: ExcelComponentOptions): IClassComponent
}

interface IClassComponent {
    toHTML(): string

    $emit(eventName: string, arg: unknown): void

    $subscribe(eventName: string, callback: IListener): void

    init(): void

    destroy(): void
}

type ExcelComponentOptions = {
    name?: string
    listeners?: EventType[]
    emitter: IEmitter
}
