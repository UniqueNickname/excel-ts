type IListener = (event: T) => void

type IListeners = {
    [x: string]: IListener[]
}

interface IEmitter {
    emit(eventName: string, arg: unknown): void

    subscribe(eventName: string, callback: IListener): IUnsubscribe
}

type IUnsubscribe = () => void
