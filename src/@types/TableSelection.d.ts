interface ITableSelection {
    groupe: DomElement[]

    $current: DomElement | undefined

    clear(): void

    select($el: DomElement): DomElement

    selectGroupe($cells: DomElement[]): void
}
