type DomListenerMethod = (event: Event) => void

interface IAbstractDomListener {
    onInput?: DomListenerMethod
    onClick?: DomListenerMethod
    onMousedown?: DomListenerMethod
    onKeydown?: DomListenerMethod
}

type EventType = 'click' | 'input' | 'mousedown' | 'keydown'
